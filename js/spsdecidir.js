var Change_Option
	 Change_Option = { 
		bank_Element: $('#edit-uc-spsdecidir-payment-bank'),
		method_Element  : $('#edit-uc-spsdecidir-payment-method'),
		fees_Element  : $('#edit-uc-spsdecidir-payment-fee'),
		Page	: "/e/spsdecidir_payment_method/",
		_Ajax: function(){ 
			 this.method_Element.attr('disabled','disabled'); 
			 this.fees_Element.attr('disabled','disabled'); 
			 this.Ajax_Call(); 
			 	

		},
		_Ajax_fees: function(){ 

			 this.fees_Element.attr('disabled','disabled'); 
			 this.Ajax_Call_fee(); 
			 	

		},
		Get_bank:	function(){ 
			return  this.bank_Element.val(); 		
		},
		Get_method:	function(){ 
			return  this.method_Element.val(); 		
		},
		Get_fee:	function(){ 
			return  this.fees_Element.val(); 		
		},
		Get_nid:	function(){ 
			return  this.nid_Element.val(); 		
		},
		Ajax_Call: function(){ 
			var Self = this;

			$.get( this.Page+this.Get_bank(), null, function(json_response){ 
				Self.Create_Option(json_response,Self.method_Element); 
				Self.method_Element.removeAttr('disabled'); 
				Change_Option.load_fees();
			}); 
		}, 
		Ajax_Call_fee: function(){ 
			var Self = this;
			$.get( this.Page+this.Get_bank() +"/" +this.Get_method(), null, function(json_response){ 
				Self.Create_Option(json_response,Self.fees_Element); 
				Self.fees_Element.removeAttr('disabled'); 
			}); 
		}, 
		ajax_set_fees: function(){ 
			var Self = this;
			$.get( this.Page+this.Get_bank() +"/" +this.Get_method() +"/" +this.Get_fee() , null, function(json_response){ 
					window.location.reload();
					return false;
				}); 
		},
                 Ajax_Call_calcuotas: function(){ 
			var Self = this;
                        $('.value','#valor_cuota').html('--');
                        $('.value','#calcuotas_monto_final').html('--');

			$.get( '/e/spsdecidir_calcuotas/'+this.Get_nid() +"/"  +this.Get_fee() , null, function(json_response){ 
    			         var _Json = Drupal.parseJson(json_response);

                                         $('.value','#valor_cuota').html(_Json.valor_cuota);
                                         $('.value','#calcuotas_monto_final').html(_Json.monto_final);
					return false;
				}); 
		}, 
 
		Create_Option:function(_json_response, element){ 
			 var _Json = Drupal.parseJson(_json_response);
			 if(_Json.empty){ 
				element.html(this.Create_Option_Node('','sin informacion')); 
			 }else {  
				 if (this.Get_bank() == '') 
					element.html(this.Create_Option_Node('','All Product'));
				 element.html('')
				 for (i in _Json ) { 
					element.append(this.Create_Option_Node(i,_Json[i]));
 
				 }
			 } 
		},
		Create_Option_Node:function(value,html){ 
			var _option = document.createElement('option');
			_option.value =  value; 
			_option.innerHTML = html;
			return _option; 
 
		},
		load_methods:function(){
			if(Change_Option.Get_bank() != '--'){
				
				Change_Option._Ajax();
			}
		},
		load_fees:function(){
			if(Change_Option.Get_method() != '--'){
				Change_Option._Ajax_fees();
			}
		},
		set_fees:function(){
			if(Change_Option.Get_fee() != '--'){
				Change_Option.ajax_set_fees();
			}
		},
		set_calcuotas:function(){
			if(Change_Option.Get_fee() != '--'){
                            Change_Option.Ajax_Call_calcuotas();
			}
		}
 
	}
	/*Change_Option.bank_Element.change(function(){ 
		Change_Option.load_methods(); //event handler
	}); 
	Change_Option.method_Element.change(function(){ 
		Change_Option.load_fees(); //event handler
	}); 
	Change_Option.fees_Element.change(function(){ 
		
		Change_Option.set_fees(); //event handler
	});*/
	
	//Change_Option.load_methods();
